defmodule TTAdminUI.LayoutView do
  use Phoenix.View, root: Path.expand("#{__DIR__}/templates"), namespace: TTAdminUI

  alias TTAdminUI.Router.Helpers, as: Routes

  def config(), do: {__MODULE__, "live.html"}
end
