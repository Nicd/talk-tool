defmodule TTAdminUI.Endpoint do
  use Phoenix.Endpoint, otp_app: :talk_tool

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  @session_options [
    store: :cookie,
    key: "_talk_tool_admin_key",
    signing_salt: "whoahman"
  ]

  socket("/live", Phoenix.LiveView.Socket, websocket: [connect_info: [session: @session_options]])

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug(Plug.Static,
    at: "/static/",
    from: "priv/static",
    gzip: false,
    only: ~w(admin client common)
  )

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket("/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket)
    plug(Phoenix.LiveReloader)
    plug(Phoenix.CodeReloader)
  end

  plug(Plug.RequestId)

  plug(Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()
  )

  plug(Plug.MethodOverride)
  plug(Plug.Head)
  plug(Plug.Session, @session_options)
  plug(TTAdminUI.Router)
end

# defmodule TTAdminUI.Endpoint do
#   import TalkTool.PlugHelpers

#   @behaviour Phoenix.Endpoint
#   @behaviour Plug

#   @config Phoenix.Endpoint.Supervisor.config(:talk_tool, __MODULE__)

#   @plugs [
#     if @config[:code_reloader] do
#       build_plug(Phoenix.LiveReloader)
#     end,
#     if @config[:code_reloader] do
#       build_plug(Phoenix.CodeReloader)
#     end,
#     build_plug(TTAdminUI.Router)
#   ]

#   @impl Phoenix.Endpoint
#   def init(_key, config) do
#     {:ok, config}
#   end

#   @impl Plug
#   def init(opts) do
#     opts
#   end

#   @impl Plug
#   def call(conn, _opts) do
#     run_plug(@plugs, conn)
#   end

#   defp run_plug(plugs, conn)

#   defp run_plug([], conn), do: conn

#   defp run_plug([nil | rest], conn), do: run_plug(rest, conn)

#   defp run_plug([{plug, opts} | rest], conn) do
#     case plug.(conn, opts) do
#       %Plug.Conn{halted: true} = new_conn ->
#         new_conn

#       %Plug.Conn{} = new_conn ->
#         run_plug(rest, new_conn)

#       other ->
#         raise "Plug did not return Plug.Conn, instead got: #{inspect(other)}"
#     end
#   end

#   defp pubsub_server!() do
#     Keyword.fetch!(@config, :pubsub_server)
#   end
# end
