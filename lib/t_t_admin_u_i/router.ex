defmodule TTAdminUI.Router do
  use Phoenix.Router
  import Phoenix.LiveView.Router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:put_root_layout, {TTAdminUI.LayoutView, :root})
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(TTUICommon.FLoCPlug)
  end

  scope "/" do
    pipe_through(:browser)

    live("/", TTAdminUI.Live.Main)
    live("/create", TTAdminUI.Live.CreatePresentation)
    live("/presentation/:id", TTAdminUI.Live.ViewPresentation)
  end
end
