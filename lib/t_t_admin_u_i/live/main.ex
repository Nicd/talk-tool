defmodule TTAdminUI.Live.Main do
  use Phoenix.LiveView, layout: TTAdminUI.LayoutView.config()
  use Phoenix.HTML
  import Phoenix.LiveView.Helpers
  import Phoenix.View
  alias TTAdminUI.Router.Helpers, as: Routes

  alias TTAdmin.Storage.DB

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    presentations = DB.get_presentations()

    {:ok, assign(socket, presentations: presentations, page_title: "Presentations")}
  end
end
