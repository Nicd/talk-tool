defmodule TTAdminUI.Live.ViewPresentation do
  use Phoenix.LiveView, layout: TTAdminUI.LayoutView.config()
  use Phoenix.HTML
  import Phoenix.LiveView.Helpers
  import Phoenix.View
  alias TTAdminUI.Router.Helpers, as: Routes
  alias Phoenix.LiveView.JS

  alias TTAdmin.Storage.DB
  alias TTAdmin.Presentation.PubSub
  alias TTAdmin.Presentation.Server
  alias TTAdmin.Schemas.Presentation

  @impl Phoenix.LiveView
  def mount(params, _session, socket) do
    server_pid = Server.get_server!(Map.get(params, "id"))
    presentation = Server.get(server_pid)

    questions = DB.get_questions(presentation) |> sort()

    if connected?(socket) do
      PubSub.listen_presentation(presentation)
      PubSub.listen_reactions(presentation)
      PubSub.listen_questions(presentation)
      Server.set_admin(server_pid, self())
    end

    {:ok,
     assign(socket,
       page_title: presentation.name,
       presentation: presentation,
       server_pid: server_pid,
       reactions: [],
       questions: questions,
       waiting_screenshot: []
     ), temporary_assigns: [reactions: []]}
  end

  @impl Phoenix.LiveView
  def handle_event(event, params, socket)

  def handle_event("got-screenshot-perm", %{"has" => has}, socket) do
    changeset =
      Presentation.update_screenshot(socket.assigns.presentation, %{can_screenshot: has})

    socket =
      case Server.update(socket.assigns.server_pid, changeset) do
        {:ok, presentation} -> assign(socket, presentation: presentation)
        {:error, _error} -> socket
      end

    {:noreply, socket}
  end

  def handle_event("took-screenshot", %{"data" => data}, socket) do
    socket =
      if socket.assigns.waiting_screenshot != [] do
        for waiter <- socket.assigns.waiting_screenshot do
          GenServer.reply(waiter, data)
        end

        assign(socket, waiting_screenshot: [])
      else
        socket
      end

    {:noreply, socket}
  end

  def handle_event("answer-question", %{"value" => id}, socket) do
    questions =
      update_question(
        socket.assigns.presentation,
        socket.assigns.questions,
        id,
        &%{&1 | answered: true}
      )

    {:noreply, assign(socket, questions: questions)}
  end

  def handle_event("unanswer-question", %{"value" => id}, socket) do
    questions =
      update_question(
        socket.assigns.presentation,
        socket.assigns.questions,
        id,
        &%{&1 | answered: false}
      )

    {:noreply, assign(socket, questions: questions)}
  end

  def handle_event("remove-question", %{"value" => id}, socket) do
    DB.remove_question(socket.assigns.presentation, id)
    questions = Enum.reject(socket.assigns.questions, &(&1.id == id))
    {:noreply, assign(socket, questions: questions)}
  end

  @impl Phoenix.LiveView
  def handle_call(msg, from, socket)

  def handle_call(:take_screenshot, from, socket) do
    socket =
      if socket.assigns.waiting_screenshot == [] do
        socket |> push_event("take-screenshot", %{}) |> assign(waiting_screenshot: [from])
      else
        assign(socket, waiting_screenshot: [from | socket.assigns.waiting_screenshot])
      end

    {:noreply, socket}
  end

  @impl Phoenix.LiveView
  def handle_info(msg, socket)

  def handle_info({:update_presentation, presentation}, socket) do
    {:noreply, assign(socket, presentation: presentation)}
  end

  def handle_info({:new_reaction, _id, reaction}, socket) do
    {:noreply, assign(socket, reactions: [reaction])}
  end

  def handle_info({:new_question, _id, question}, socket) do
    {:noreply, assign(socket, questions: socket.assigns.questions ++ [question])}
  end

  @impl Phoenix.LiveView
  def terminate(_reason, socket) do
    changeset =
      Presentation.update_screenshot(socket.assigns.presentation, %{can_screenshot: false})

    Server.update(socket.assigns.server_pid, changeset)
    Server.set_admin(socket.assigns.server_pid, nil)
  end

  @spec update_question(
          Presentation.t(),
          [TTAdmin.Schemas.Question.t()],
          TTAdmin.Schemas.Question.id(),
          (TTAdmin.Schemas.Question.t() -> TTAdmin.Schemas.Question.t())
        ) :: [TTAdmin.Schemas.Question.t()]
  defp update_question(presentation, questions, id, op) do
    question = Enum.find(questions, &(&1.id == id))

    if question != nil do
      question = op.(question)
      DB.put_question(presentation, question)
      questions |> Enum.reject(&(&1.id == id)) |> then(&[question | &1]) |> sort()
    else
      questions
    end
  end

  @spec reaction_id(TTAdmin.Schemas.Reaction.t()) :: String.t()
  defp reaction_id(reaction) do
    "emoji-reaction-#{reaction.id}"
  end

  @spec sort([TTAdmin.Schemas.Question.t()]) :: [TTAdmin.Schemas.Question.t()]
  defp sort(questions) do
    Enum.sort_by(questions, & &1.at, {:asc, DateTime})
  end
end
