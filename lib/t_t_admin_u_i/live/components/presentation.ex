defmodule TTAdminUI.Live.Components.Presentation do
  use Phoenix.Component

  alias TTClientUI.Router.Helpers, as: ClientRoutes

  def question(assigns) do
    ~H"""
    <div class={if(@question.answered, do: "question answered", else: "question")}>
      <p class="question-text"><%= @question.text %></p>

      <%= if @question.picture do %>
        <img src={@question.picture} class="screenshot" />
      <% end %>

      <%= if not @question.answered do %>
        <button type="button" phx-click="answer-question" value={@question.id}>✓</button>
      <% else %>
        <button type="button" phx-click="unanswer-question" value={@question.id}>❌</button>
      <% end %>

      <button type="button" phx-click="remove-question" value={@question.id}>🗑</button>
    </div>
    """
  end

  def qr(assigns) do
    ~H"""
    <img
      src={"data:image/svg+xml;base64,#{QRCode.create!(ClientRoutes.live_url(TTClientUI.Endpoint, TTClientUI.Live.ViewPresentation, @presentation.id)) |> QRCode.Svg.to_base64()}"}
      class="qr"
    />
    """
  end
end
