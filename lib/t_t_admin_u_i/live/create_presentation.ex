defmodule TTAdminUI.Live.CreatePresentation do
  use Phoenix.LiveView, layout: TTAdminUI.LayoutView.config()
  use Phoenix.HTML
  import Phoenix.LiveView.Helpers
  import Phoenix.View
  alias TTAdminUI.Router.Helpers, as: Routes

  alias TTAdmin.Schemas.Presentation
  alias TTAdmin.Storage.DB

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    loading = not Phoenix.LiveView.connected?(socket)

    changeset =
      if not loading do
        Presentation.create_changeset(%{})
      end

    {:ok,
     assign(socket,
       page_title: "Create New",
       loading: loading,
       changeset: changeset,
       submit_error: false
     )}
  end

  @impl Phoenix.LiveView
  def handle_event(evt, params, socket)

  def handle_event("change", %{"presentation" => presentation}, socket) do
    changeset = Presentation.create_changeset(presentation)

    {:noreply, assign(socket, changeset: changeset)}
  end

  def handle_event("submit", %{"presentation" => presentation}, socket) do
    changeset = Presentation.create_changeset(presentation)

    case Ecto.Changeset.apply_action(changeset, :create) do
      {:ok, presentation} ->
        DB.put_presentation(presentation)

        {:noreply,
         push_navigate(socket,
           to: Routes.live_path(socket, TTAdminUI.Live.ViewPresentation, presentation.id)
         )}

      {:error, changeset} ->
        {:noreply, assign(socket, changeset: changeset, submit_error: true)}
    end
  end
end
