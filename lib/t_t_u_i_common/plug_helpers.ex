defmodule TTUICommon.PlugHelpers do
  @spec build_plug(module(), Plug.opts()) :: {module(), Plug.opts()}
  def build_plug(plug_mod, opts \\ []) do
    {plug_mod, plug_mod.init(opts)}
  end
end
