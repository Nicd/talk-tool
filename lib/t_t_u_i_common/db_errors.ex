defimpl Plug.Exception, for: TTAdmin.Storage.DB.NoResultsError do
  def status(_exception), do: 404
  def actions(_exception), do: []
end
