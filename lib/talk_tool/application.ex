defmodule TalkTool.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl Application
  def start(_type, _args) do
    children = [
      Supervisor.child_spec({Phoenix.PubSub, name: TTAdmin.Presentation.PubSub},
        id: TTAdmin.Presentation.PubSub
      ),
      {Registry, keys: :unique, name: TTAdmin.Presentation.Server.Registry},
      {DynamicSupervisor, strategy: :one_for_one, name: TTAdmin.Presentation.Server.Supervisor},
      {CubDB, data_dir: Application.fetch_env!(:talk_tool, :db_dir), name: TTAdmin.Storage.DB},
      Supervisor.child_spec({Phoenix.PubSub, name: TTAdminUI.PubSub}, id: TTAdminUI.PubSub),
      TTAdminUI.Endpoint,
      Supervisor.child_spec({Phoenix.PubSub, name: TTClientUI.PubSub}, id: TTClientUI.PubSub),
      TTClientUI.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TalkTool.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
