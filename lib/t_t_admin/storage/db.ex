defmodule TTAdmin.Storage.DB do
  alias TTAdmin.Schemas
  alias Ecto.Changeset

  defmodule NoResultsError do
    defexception [:message]
  end

  @spec get_presentations() :: [Schemas.Presentation.t()]
  def get_presentations() do
    CubDB.get(__MODULE__, :presentations, []) |> Enum.map(&load_presentation/1)
  end

  @spec get_presentation!(Schemas.Presentation.id()) :: Schemas.Presentation.t() | no_return()
  def get_presentation!(id) do
    p =
      get_presentations()
      |> Enum.find(&(&1.id == id))

    if p != nil do
      load_presentation(p)
    else
      raise NoResultsError, "Cannot find presentation with id #{inspect(id)}"
    end
  end

  @spec create_presentation(Changeset.t()) ::
          {:ok, Schemas.Presentation.t()} | {:error, Changeset.t()}
  def create_presentation(changeset) do
    case Changeset.apply_action(changeset, :insert) do
      {:ok, data} ->
        CubDB.update(__MODULE__, :presentations, [data], fn old ->
          [data | old]
        end)

        {:ok, data}

      {:error, cset} ->
        {:error, cset}
    end
  end

  @spec put_presentation(Schemas.Presentation.t()) :: :ok
  def put_presentation(presentation) do
    presentation = %{
      id: presentation.id,
      name: presentation.name
    }

    CubDB.update(__MODULE__, :presentations, [presentation], fn old ->
      i = Enum.find_index(old, &(&1.id == presentation.id))

      if i != nil do
        List.replace_at(old, i, presentation)
      else
        [presentation | old]
      end
    end)
  end

  @spec create_reaction(Changeset.t()) :: {:ok, Schemas.Reaction.t()} | {:error, Changeset.t()}
  def create_reaction(changeset) do
    case Changeset.apply_action(changeset, :insert) do
      {:ok, data} ->
        CubDB.update(__MODULE__, reactions_id(data.presentation_id), [data], fn old ->
          [data | old]
        end)

        {:ok, data}

      {:error, cset} ->
        {:error, cset}
    end
  end

  @spec get_questions(Schemas.Presentation.t()) :: [Schemas.Question.t()]
  def get_questions(presentation) do
    CubDB.get(__MODULE__, questions_id(presentation.id), [])
  end

  @spec create_question(Changeset.t()) :: {:ok, Schemas.Question.t()} | {:error, Changeset.t()}
  def create_question(changeset) do
    case Changeset.apply_action(changeset, :insert) do
      {:ok, data} ->
        CubDB.update(__MODULE__, questions_id(data.presentation_id), [data], fn old ->
          [data | old]
        end)

        {:ok, data}

      {:error, cset} ->
        {:error, cset}
    end
  end

  @spec put_question(Schemas.Presentation.t(), Schemas.Question.t()) :: :ok
  def put_question(presentation, question) do
    CubDB.update(__MODULE__, questions_id(presentation.id), [question], fn old ->
      i = Enum.find_index(old, &(&1.id == question.id))

      if i != nil do
        List.replace_at(old, i, question)
      else
        [question | old]
      end
    end)
  end

  @spec remove_question(Schemas.Presentation.t(), Schemas.Question.id()) :: :ok
  def remove_question(presentation, id) do
    CubDB.update(__MODULE__, questions_id(presentation.id), [], fn old ->
      Enum.reject(old, &(&1.id == id))
    end)
  end

  @spec load_presentation(map()) :: Presentation.t()
  defp load_presentation(data) do
    %Schemas.Presentation{
      id: data.id,
      name: data.name,
      can_screenshot: false
    }
  end

  @spec reactions_id(Schemas.Presentation.id()) :: String.t()
  defp reactions_id(id) do
    "reactions-#{id}"
  end

  @spec questions_id(Schemas.Presentation.id()) :: String.t()
  defp questions_id(id) do
    "questions-#{id}"
  end
end
