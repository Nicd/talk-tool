defmodule TTAdmin.Presentation.PubSub do
  alias Phoenix.PubSub
  alias TTAdmin.Schemas.Reaction
  alias TTAdmin.Schemas.Presentation
  alias TTAdmin.Schemas.Question

  @spec publish_presentation(Presentation.t()) :: :ok
  def publish_presentation(presentation) do
    PubSub.broadcast!(
      __MODULE__,
      "#{presentation.id}",
      {:update_presentation, presentation}
    )
  end

  @spec listen_presentation(Presentation.t()) :: :ok | {:error, term()}
  def listen_presentation(presentation) do
    PubSub.subscribe(__MODULE__, "#{presentation.id}")
  end

  @spec publish_admin_pid(Presentation.t(), pid() | nil) :: :ok
  def publish_admin_pid(presentation, pid) do
    PubSub.broadcast!(__MODULE__, "#{presentation.id}:pid", {:new_admin_pid, pid})
  end

  @spec listen_admin_pid(Presentation.t()) :: :ok | {:error, term()}
  def listen_admin_pid(presentation) do
    PubSub.subscribe(__MODULE__, "#{presentation.id}:pid")
  end

  @spec publish_reaction(Reaction.t()) :: :ok
  def publish_reaction(reaction) do
    PubSub.broadcast!(
      __MODULE__,
      "#{reaction.presentation_id}:reactions",
      {:new_reaction, reaction.presentation_id, reaction}
    )
  end

  @spec listen_reactions(Presentation.t()) :: :ok | {:error, term()}
  def listen_reactions(presentation) do
    PubSub.subscribe(__MODULE__, "#{presentation.id}:reactions")
  end

  @spec publish_question(Question.t()) :: :ok
  def publish_question(question) do
    PubSub.broadcast!(
      __MODULE__,
      "#{question.presentation_id}:questions",
      {:new_question, question.presentation_id, question}
    )
  end

  @spec listen_questions(Presentation.t()) :: :ok | {:error, term()}
  def listen_questions(presentation) do
    PubSub.subscribe(__MODULE__, "#{presentation.id}:questions")
  end
end
