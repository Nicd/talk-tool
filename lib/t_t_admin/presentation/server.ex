defmodule TTAdmin.Presentation.Server do
  use GenServer, restart: :transient

  import TalkTool.TypedStruct

  alias TTAdmin.Schemas.Presentation
  alias TTAdmin.Storage.DB
  alias TTAdmin.Presentation.PubSub

  defmodule Options do
    deftypedstruct(%{
      id: Presentation.id()
    })
  end

  defmodule State do
    deftypedstruct(%{
      presentation: Presentation.t(),
      admin: {pid() | nil, nil}
    })
  end

  @spec start_link(Options.t()) :: GenServer.on_start()
  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts.id, name: via(opts.id))
  end

  @impl GenServer
  def init(id) do
    presentation = DB.get_presentation!(id)

    {:ok, %State{presentation: presentation}}
  end

  @impl GenServer
  def handle_call(msg, from, state)

  def handle_call(:get, _from, state) do
    {:reply, state.presentation, state}
  end

  def handle_call(:get_admin, _from, state) do
    {:reply, state.admin, state}
  end

  def handle_call({:update, changeset}, _from, state) do
    case Ecto.Changeset.apply_action(changeset, :update) do
      {:ok, presentation} ->
        DB.put_presentation(presentation)
        PubSub.publish_presentation(presentation)
        {:reply, {:ok, presentation}, %State{state | presentation: presentation}}

      {:error, changeset} ->
        {:reply, {:error, changeset}, state}
    end
  end

  def handle_call({:set_admin, pid}, from, state) do
    new_admin =
      cond do
        pid == nil and state.admin == from -> nil
        pid != nil -> pid
        true -> state.admin
      end

    PubSub.publish_admin_pid(state.presentation, new_admin)
    {:reply, :ok, %State{state | admin: new_admin}}
  end

  @spec get_server!(Presentation.id()) :: GenServer.name()
  def get_server!(id) do
    case Registry.lookup(__MODULE__.Registry, id) do
      [{pid, _}] ->
        pid

      _ ->
        case DynamicSupervisor.start_child(__MODULE__.Supervisor, {__MODULE__, %Options{id: id}}) do
          {:ok, pid} -> pid
          {:error, {:already_started, pid}} -> pid
          {:error, error} -> raise DB.NoResultsError, error
        end
    end
  end

  @spec get(GenServer.name()) :: Presentation.t()
  def get(server) do
    GenServer.call(server, :get)
  end

  @spec update(GenServer.name(), Ecto.Changeset.t()) ::
          {:ok, Presentation.t()} | {:error, Ecto.Changeset.t()}
  def update(server, changeset) do
    GenServer.call(server, {:update, changeset})
  end

  @spec get_admin(GenServer.name()) :: pid() | nil
  def get_admin(server) do
    GenServer.call(server, :get_admin)
  end

  @spec set_admin(GenServer.name(), pid() | nil) :: :ok
  def set_admin(server, pid) do
    GenServer.call(server, {:set_admin, pid})
  end

  @spec via(Presentation.id()) :: GenServer.name()
  defp via(id), do: {:via, Registry, {__MODULE__.Registry, id}}
end
