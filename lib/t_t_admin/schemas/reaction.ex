defmodule TTAdmin.Schemas.Reaction do
  import TalkTool.TypedSchema

  alias Ecto.Changeset
  alias TTAdmin.Schemas.Presentation

  @type id() :: Ecto.UUID.t()

  deftypedschema(:embedded) do
    field(:presentation_id, Ecto.UUID, Presentation.id() | nil)

    field(:emoji, :string, String.t())
    field(:at, :utc_datetime, DateTime.t())
  end

  @spec create_changeset(Presentation.t(), map()) :: Changeset.t()
  def create_changeset(presentation, params) do
    %__MODULE__{}
    |> Changeset.cast(params, [:emoji])
    |> Changeset.validate_required([:emoji])
    |> Changeset.validate_length(:emoji, is: 1)
    |> Changeset.put_change(:presentation_id, presentation.id)
    |> Changeset.put_change(:at, DateTime.utc_now() |> DateTime.truncate(:second))
    |> Changeset.put_change(:id, Ecto.UUID.autogenerate())
  end
end
