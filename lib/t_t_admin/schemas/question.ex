defmodule TTAdmin.Schemas.Question do
  import TalkTool.TypedSchema

  alias Ecto.Changeset
  alias TTAdmin.Schemas.Presentation

  @type id() :: Ecto.UUID.t()

  deftypedschema(:embedded) do
    field(:presentation_id, Ecto.UUID, Presentation.id() | nil)

    field(:text, :string, String.t())
    field(:at, :utc_datetime, DateTime.t())
    field(:answered, :boolean, boolean())
    field(:picture, :binary, binary() | nil)
  end

  @spec create_changeset(Presentation.t(), map()) :: Changeset.t()
  def create_changeset(presentation, params) do
    %__MODULE__{}
    |> Changeset.cast(params, [:text, :picture])
    |> Changeset.validate_required([:text])
    |> Changeset.put_change(:presentation_id, presentation.id)
    |> Changeset.put_change(:at, DateTime.utc_now() |> DateTime.truncate(:second))
    |> Changeset.put_change(:id, Ecto.UUID.autogenerate())
    |> Changeset.put_change(:answered, false)
  end
end
