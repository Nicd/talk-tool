defmodule TTAdmin.Schemas.Presentation do
  import TalkTool.TypedSchema

  alias Ecto.Changeset

  @type id() :: Ecto.UUID.t()

  deftypedschema(:embedded) do
    field(:name, :string, String.t())
    field(:can_screenshot, :boolean, boolean())
  end

  @spec create_changeset(map()) :: Changeset.t()
  def create_changeset(params) do
    %__MODULE__{}
    |> Changeset.cast(params, [:name])
    |> Changeset.validate_required([:name])
    |> Changeset.put_change(:id, Ecto.UUID.autogenerate())
    |> Changeset.put_change(:can_screenshot, false)
  end

  @spec update_screenshot(t(), map()) :: Changeset.t()
  def update_screenshot(presentation, params) do
    presentation
    |> Changeset.cast(params, [:can_screenshot])
    |> Changeset.validate_required([:can_screenshot])
    |> Changeset.validate_change(:can_screenshot, fn
      :can_screenshot, val when is_boolean(val) -> []
      :can_screenshot, _ -> [can_screenshot: "Must be boolean."]
    end)
  end
end
