defmodule TTClientUI.ErrorView do
  use Phoenix.View, root: Path.expand("#{__DIR__}/templates"), namespace: TTClientUI

  alias TTClientUI.Router.Helpers, as: Routes
end
