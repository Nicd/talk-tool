defmodule TTClientUI.LayoutView do
  use Phoenix.View, root: Path.expand("#{__DIR__}/templates"), namespace: TTClientUI

  alias TTClientUI.Router.Helpers, as: Routes

  def config(), do: {__MODULE__, "live.html"}
end
