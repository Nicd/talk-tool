defmodule TTClientUI.Live.ViewPresentation do
  use Phoenix.LiveView, layout: TTClientUI.LayoutView.config()
  use Phoenix.HTML
  import Phoenix.LiveView.Helpers
  import Phoenix.View
  alias TTClientUI.Router.Helpers, as: Routes
  alias Phoenix.LiveView.JS

  alias TTAdmin.Schemas.Reaction
  alias TTAdmin.Storage.DB
  alias TTAdmin.Presentation.Server

  @impl Phoenix.LiveView
  def mount(params, _session, socket) do
    server_pid = Server.get_server!(Map.get(params, "id"))
    presentation = Server.get(server_pid)

    if connected?(socket) do
      TTAdmin.Presentation.PubSub.listen_presentation(presentation)
    end

    {:ok,
     assign(socket,
       page_title: presentation.name,
       presentation: presentation
     )}
  end

  @impl Phoenix.LiveView
  def handle_event(evt, params, socket)

  def handle_event("emoji-select", %{"value" => emoji}, socket) do
    changeset = Reaction.create_changeset(socket.assigns.presentation, %{emoji: emoji})

    if changeset.valid? do
      case DB.create_reaction(changeset) do
        {:ok, reaction} -> TTAdmin.Presentation.PubSub.publish_reaction(reaction)
        _ -> :ok
      end
    end

    {:noreply, socket}
  end

  @impl Phoenix.LiveView
  def handle_info(msg, socket)

  def handle_info({:update_presentation, presentation}, socket) do
    {:noreply, assign(socket, presentation: presentation)}
  end
end
