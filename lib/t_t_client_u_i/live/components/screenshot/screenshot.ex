defmodule TTClientUI.Live.Components.Screenshot do
  use Phoenix.LiveComponent

  @impl Phoenix.LiveComponent
  def mount(socket) do
    {:ok,
     assign(socket,
       picture: nil
     )}
  end

  @impl Phoenix.LiveComponent
  def handle_event(event, params, socket)

  def handle_event("take-screenshot", _params, socket) do
    if socket.assigns.admin_pid != nil do
      picture = GenServer.call(socket.assigns.admin_pid, :take_screenshot)
      {:noreply, assign(socket, picture: picture)}
    else
      {:noreply, socket}
    end
  end
end
