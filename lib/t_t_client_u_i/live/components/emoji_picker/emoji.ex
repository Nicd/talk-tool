defmodule TTClientUI.Live.Components.EmojiPicker.Emoji do
  import TalkTool.TypedStruct

  deftypedstruct(%{
    char: String.t(),
    name: String.t()
  })

  defimpl String.Chars, for: __MODULE__ do
    @spec to_string(TTClientUI.Live.Components.EmojiPicker.Emoji.t()) :: String.t()
    def to_string(emoji), do: emoji.char
  end

  defimpl Phoenix.HTML.Safe, for: __MODULE__ do
    @spec to_iodata(TTClientUI.Live.Components.EmojiPicker.Emoji.t()) :: String.t()
    def to_iodata(emoji), do: emoji.char
  end
end
