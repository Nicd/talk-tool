defmodule TTClientUI.Live.Components.EmojiPicker do
  use Phoenix.LiveComponent

  alias __MODULE__.Data
  alias Phoenix.LiveView.JS

  @impl Phoenix.LiveComponent
  def mount(socket) do
    categories = Data.categories()
    emojis = Data.emojis()
    {:ok, assign(socket, open_view: nil, categories: categories, emojis: emojis)}
  end

  @impl Phoenix.LiveComponent
  def render(assigns) do
    ~H"""
    <div id={element_id(@id)} class="emoji-picker" phx-hook="EmojiPicker">
      <div class="emoji-picker-categories">
        <%= for c <- @categories do %>
          <button
            type="button"
            phx-click={
              JS.dispatch("scroll",
                to: "#" <> element_id(@id),
                detail: %{id: category_id(@id, c)}
              )
            }
            phx-target="@myself"
            value={c}
          >
            <%= Data.category_icon(c) %>
          </button>
        <% end %>
      </div>

      <div id={content_id(@id)} class="emoji-picker-content">
        <%= for c <- @categories do %>
          <div id={category_id(@id, c)} class="emoji-picker-category">
            <%= for emoji <- Map.fetch!(@emojis, c) do %>
              <button type="button" phx-click="emoji-select" value={emoji.char}>
                <%= emoji %>
              </button>
            <% end %>
          </div>
        <% end %>
      </div>
    </div>
    """
  end

  @spec element_id(String.t()) :: String.t()
  defp element_id(id), do: "emoji-picker-#{id}"

  @spec content_id(String.t()) :: String.t()
  defp content_id(id), do: "#{element_id(id)}-content"

  @spec category_id(String.t(), Data.category()) :: String.t()
  defp category_id(id, category), do: "#{element_id(id)}-category-#{category}"
end
