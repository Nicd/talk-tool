defmodule TTClientUI.Live.AskQuestion do
  use Phoenix.LiveView, layout: TTClientUI.LayoutView.config()
  use Phoenix.HTML
  import Phoenix.LiveView.Helpers
  import Phoenix.View
  alias TTClientUI.Router.Helpers, as: Routes

  alias TTAdmin.Schemas.Question
  alias TTAdmin.Storage.DB
  alias TTAdmin.Presentation.Server
  alias TTAdmin.Presentation.PubSub

  @impl Phoenix.LiveView
  def mount(params, _session, socket) do
    server_pid = Server.get_server!(Map.get(params, "id"))
    presentation = Server.get(server_pid)
    admin_pid = Server.get_admin(server_pid)
    changeset = Question.create_changeset(presentation, %{})

    if connected?(socket) do
      PubSub.listen_presentation(presentation)
      PubSub.listen_admin_pid(presentation)
    end

    {:ok,
     assign(socket,
       page_title: presentation.name,
       presentation: presentation,
       admin_pid: admin_pid,
       changeset: changeset,
       picture: nil
     )}
  end

  @impl Phoenix.LiveView
  def handle_event(evt, params, socket)

  def handle_event("change", %{"question" => question}, socket) do
    changeset = Question.create_changeset(socket.assigns.presentation, question)

    {:noreply, assign(socket, changeset: changeset)}
  end

  def handle_event("submit", %{"question" => question}, socket) do
    changeset =
      Question.create_changeset(
        socket.assigns.presentation,
        Map.put(question, "picture", socket.assigns.picture)
      )

    case DB.create_question(changeset) do
      {:ok, question} ->
        TTAdmin.Presentation.PubSub.publish_question(question)

        {:noreply,
         push_navigate(socket,
           to:
             Routes.live_path(socket, TTClientUI.Live.ViewPresentation, question.presentation_id)
         )}

      {:error, changeset} ->
        {:noreply, assign(socket, changeset: changeset, submit_error: true)}
    end
  end

  def handle_event("select-screenshot", %{"value" => data}, socket) do
    {:noreply, assign(socket, picture: data)}
  end

  def handle_event("remove-screenshot", _params, socket) do
    {:noreply, assign(socket, picture: nil)}
  end

  @impl Phoenix.LiveView
  def handle_info(msg, socket)

  def handle_info({:update_presentation, presentation}, socket) do
    {:noreply, assign(socket, presentation: presentation)}
  end

  def handle_info({:new_admin_pid, pid}, socket) do
    {:noreply, assign(socket, admin_pid: pid)}
  end
end
