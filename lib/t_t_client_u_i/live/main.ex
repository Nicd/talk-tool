defmodule TTClientUI.Live.Main do
  use Phoenix.LiveView, layout: {TTClientUI.LayoutView, "live.html"}
  use Phoenix.HTML
  import Phoenix.LiveView.Helpers
  import Phoenix.View
  alias TTClientUI.Router.Helpers, as: Routes

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, assign(socket, page_title: "TalkTool")}
  end
end
