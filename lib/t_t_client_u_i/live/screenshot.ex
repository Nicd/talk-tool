defmodule TTClientUI.Live.Screenshot do
  use Phoenix.LiveView, layout: TTClientUI.LayoutView.config()
  use Phoenix.HTML
  import Phoenix.LiveView.Helpers
  import Phoenix.View
  alias TTClientUI.Router.Helpers, as: Routes

  alias TTAdmin.Presentation.PubSub
  alias TTAdmin.Presentation.Server

  @impl Phoenix.LiveView
  def mount(params, _session, socket) do
    server_pid = Server.get_server!(Map.get(params, "id"))
    presentation = Server.get(server_pid)
    admin_pid = Server.get_admin(server_pid)

    if connected?(socket) do
      PubSub.listen_presentation(presentation)
      PubSub.listen_admin_pid(presentation)
    end

    {:ok,
     assign(socket,
       page_title: "Grab a Screenshot",
       presentation: presentation,
       admin_pid: admin_pid,
       picture: nil
     )}
  end

  @impl Phoenix.LiveView
  def handle_info(msg, socket)

  def handle_info({:update_presentation, presentation}, socket) do
    {:noreply, assign(socket, presentation: presentation)}
  end

  def handle_info({:new_admin_pid, pid}, socket) do
    {:noreply, assign(socket, admin_pid: pid)}
  end
end
