defmodule TTClientUI.Router do
  use Phoenix.Router
  import Phoenix.LiveView.Router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:put_root_layout, {TTClientUI.LayoutView, :root})
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(TTUICommon.FLoCPlug)
  end

  scope "/" do
    pipe_through(:browser)

    live("/", TTClientUI.Live.Main)
    live("/presentation/:id", TTClientUI.Live.ViewPresentation)
    live("/presentation/:id/question", TTClientUI.Live.AskQuestion)
    live("/presentation/:id/screenshot", TTClientUI.Live.Screenshot)
  end
end
