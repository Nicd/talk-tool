import { Socket } from "../common/phx.js";
import { LiveSocket } from "../common/lv.js";
import "../common/vendor/topbar.js";
import { emojiReactionHook } from "./emoji-reaction.js";
import { askForScreenshotPerm, takeScreenshot } from "./screenshot.js";

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content");

let mediaStream = null;

const hooks = {
  EmojiReaction: emojiReactionHook,
  Screenshot: {
    mounted() {
      const check = document.getElementById("screenshot-check");
      const preview = document.getElementById("screenshot-preview");

      check.addEventListener("click", async () => {
        if (check.checked) {
          if (!mediaStream) {
            const video = document.createElement("video");
            video.setAttribute("id", "screenshot-video");
            video.setAttribute("autoplay", "autoplay");
            preview.appendChild(video);

            mediaStream = await askForScreenshotPerm();

            if (mediaStream) {
              for (const track of mediaStream.getVideoTracks()) {
                track.addEventListener("ended", () => {
                  this.pushEvent("got-screenshot-perm", { has: false });
                  mediaStream = null;
                  video.remove();
                });
              }

              video.srcObject = mediaStream;
            } else {
              check.checked = false;
              video.remove();
            }
          }

          this.pushEvent("got-screenshot-perm", { has: !!mediaStream });
        } else {
          for (const child of Array.from(preview.children)) {
            preview.removeChild(child);
          }

          if (mediaStream) {
            for (const track of mediaStream.getVideoTracks()) {
              track.stop();
              mediaStream = null;
            }
          }

          check.checked = false;

          this.pushEvent("got-screenshot-perm", { has: false });
        }
      });

      this.handleEvent("take-screenshot", () => {
        if (!mediaStream) {
          return;
        }

        const screenshot = takeScreenshot(mediaStream);
        if (screenshot) {
          this.pushEvent("took-screenshot", { data: screenshot });
        }
      });
    }
  }
};

let liveSocket = new LiveSocket("/live", Socket, { hooks, params: { _csrf_token: csrfToken } });

// Show progress bar on live navigation and form submits
topbar.config({ barColors: { 0: "#29d" }, shadowColor: "rgba(0, 0, 0, .3)" });
window.addEventListener("phx:page-loading-start", info => topbar.show());
window.addEventListener("phx:page-loading-stop", info => topbar.hide());

// connect if there are any LiveViews on the page
liveSocket.connect();

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket;

