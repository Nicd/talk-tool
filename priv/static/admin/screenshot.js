export async function askForScreenshotPerm() {
  try {
    return await navigator.mediaDevices.getDisplayMedia({ video: { cursor: "always" } });
  } catch (e) {
    console.error(e);
    return null;
  }
}

export function takeScreenshot(mediaStream) {
  const canvas = document.createElement("canvas");
  const video = document.getElementById("screenshot-video");

  try {
    const track = mediaStream.getVideoTracks()[0];

    if (track && video) {
      let { width, height } = track.getSettings();
      width = width || window.innerWidth;
      height = height || window.innerHeight;

      canvas.width = width;
      canvas.height = height;

      if (video.srcObject !== mediaStream) {
        video.srcObject = mediaStream;
      }

      const context = canvas.getContext("2d");
      context.drawImage(video, 0, 0);
      const frame = canvas.toDataURL("image/png");

      return frame;
    } else {
      return null;
    }
  } catch (e) {
    console.error(e);
    return null;
  }
}
