const EMOJI_TIME = 5_000;

export const emojiReactionHook = {
  mounted() {
    let position = Math.random() * window.innerWidth - 50;
    position = Math.max(0, position);
    position = Math.min(window.innerWidth - 120, position);
    this.el.style.left = `${position}px`;

    this.el.classList.add("fade-out");

    window.setTimeout(() => {
      if (this.el) {
        this.el.remove();
      }
    }, EMOJI_TIME);
  }
};
