export const emojiPickerHook = {
  mounted() {
    this.el.addEventListener("scroll", e => {
      const targetId = e.detail?.id;
      if (targetId) {
        const el = document.getElementById(targetId);

        if (el) {
          el.scrollIntoView();
        }
      }
    });
  }
};
