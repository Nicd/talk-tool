export const screenshotDownloadHook = {
  mounted() {
    this.el.addEventListener("click", () => {
      const img = document.getElementById("screenshot-img");

      if (img) {
        // Don't you just love web programming?!
        const a = document.createElement("a");
        a.href = img.getAttribute("src");
        a.target = "_blank";
        a.download = "screenshot.png";

        const evt = new MouseEvent("click");
        a.dispatchEvent(evt);
      }
    });
  }
};
