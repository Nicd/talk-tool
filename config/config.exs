import Config

config :talk_tool, TTAdminUI.Endpoint,
  render_errors: [accepts: ~w(html json)],
  pubsub_server: TTAdminUI.PubSub,
  code_reloader: config_env() == :dev,
  debug_errors: config_env() == :dev,
  check_origin: config_env() == :prod

config :talk_tool, TTClientUI.Endpoint,
  render_errors: [accepts: ~w(html json), root_layout: {TTClientUI.ErrorView, :root}],
  pubsub_server: TTClientUI.PubSub,
  code_reloader: config_env() == :dev,
  debug_errors: config_env() == :dev,
  check_origin: config_env() == :prod

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :talk_tool,
  compile_env: Mix.env()

config :phoenix, :json_library, Jason
