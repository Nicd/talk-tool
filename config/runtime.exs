import Config
import TalkTool.ConfigHelpers, only: [get_env: 3, get_env: 2, get_env: 1]

if Config.config_env() == :dev do
  DotenvParser.load_file(".env")
end

config :talk_tool,
  db_dir: Application.app_dir(:talk_tool, "priv") |> Path.join(get_env("DB_DIR", "db"))

config :talk_tool, TTAdminUI.Endpoint,
  http: [port: get_env("ADMIN_PORT", 6969, :int)],
  url: [
    host: get_env("ADMIN_HOST", "localhost"),
    port: get_env("ADMIN_HOST_PORT", 6969, :int),
    scheme: get_env("ADMIN_SCHEME", "http")
  ],

  # LiveView
  live_view: [signing_salt: get_env("LIVE_VIEW_SIGNING_SALT", "FxvcOwl5788vwgJD")]

config :talk_tool, TTClientUI.Endpoint,
  http: [port: get_env("CLIENT_PORT", 4242, :int)],
  url: [
    host: get_env("CLIENT_HOST", "localhost"),
    port: get_env("CLIENT_HOST_PORT", 4242, :int),
    scheme: get_env("CLIENT_SCHEME", "http")
  ],

  # LiveView
  live_view: [signing_salt: get_env("LIVE_VIEW_SIGNING_SALT", "FxvgVmWxHFVVmwDB")]

case config_env() do
  :dev ->
    config :talk_tool, TTAdminUI.Endpoint,
      secret_key_base: "asd+0f98dfasd+098dfas+0d98dfa+0sd98df0+a9s8df+09a8sdf+09a8sdf+098adf"

    config :talk_tool, TTClientUI.Endpoint,
      secret_key_base: "aw.4,5nmaw3.,4m5wrnraw3.,5mnaaw34,.m5nwaw3.,m5naw3.,m5na.w3,m5na.,w3mn5"

    config :talk_tool, TTAdminUI.Endpoint,
      live_reload: [
        patterns: [
          ~r{priv/static/admin/.*(js|s?css|png|jpeg|jpg|gif|svg)$},
          ~r{lib/(t_t_admin_u_i|talk_tool|t_t_u_i_common|t_t_admin)/.*(\.ex)$},
          ~r{lib/(t_t_admin_u_i)/.*(\.[hl]?eex)$}
        ]
      ]

    config :talk_tool, TTClientUI.Endpoint,
      live_reload: [
        patterns: [
          ~r{priv/static/client/.*(js|s?css|png|jpeg|jpg|gif|svg)$},
          ~r{lib/(t_t_client_u_i|talk_tool|t_t_u_i_common|t_t_admin)/.*(\.ex)$},
          ~r{lib/(t_t_client_u_i)/.*(\.[hl]?eex)$}
        ]
      ]

    config :phoenix, :stacktrace_depth, 20
end
