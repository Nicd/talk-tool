defmodule TalkTool.MixProject do
  use Mix.Project

  def project do
    [
      app: :talk_tool,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {TalkTool.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:phoenix, "~> 1.6.13"},
      {:phoenix_live_view, "~> 0.18.0"},
      {:phoenix_html, "~> 3.2"},
      {:phoenix_pubsub, "~> 2.1"},
      {:phoenix_view, "~> 1.1"},
      {:phoenix_live_reload, "~> 1.3"},
      {:plug_cowboy, "~> 2.5"},
      {:ecto, "~> 3.9.1"},
      {:phoenix_ecto, "~> 4.4"},
      {:jason, "~> 1.4"},
      {:dotenv_parser, "~> 2.0"},
      {:cubdb, "~> 2.0"},
      {:qr_code, "~> 2.3"}
    ]
  end
end
